package com.simplified.mimo.room



import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.simplified.mimo.room.models.LessonCompletionEvent

@Dao
interface LessonCompletionEventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLessonCompletionEvent(lessonCompletionEvent: LessonCompletionEvent)

    @Query("SELECT * FROM lessons_completion_events")
    fun loadAllCompletedLessons(): Array<LessonCompletionEvent>

}