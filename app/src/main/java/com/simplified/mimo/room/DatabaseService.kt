package com.simplified.mimo.room

import androidx.room.Room
import com.simplified.mimo.MimoApp

object DatabaseService {
    private val db = Room.databaseBuilder(
        MimoApp.appContext,
        AppDatabase::class.java, "mimo-database"
    ).build()

    val lessonCompletionEventDao = db.lessonCompletionEventDao()
}