package com.simplified.mimo.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.simplified.mimo.room.models.LessonCompletionEvent

@Database(entities = [LessonCompletionEvent::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun lessonCompletionEventDao(): LessonCompletionEventDao
}