package com.simplified.mimo.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.simplified.mimo.R
import com.simplified.mimo.databinding.FragmentHomeBinding
import com.simplified.mimo.databinding.LessonsFragmentBinding
import com.simplified.mimo.models.Lesson
import com.simplified.mimo.utils.HelperUtils

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        binding.startAgain.setOnClickListener {
            val transaction = requireActivity().supportFragmentManager.beginTransaction()
            transaction.replace(R.id.frameLayout, LessonsFragment.newInstance())
            transaction.disallowAddToBackStack()
            transaction.commit()
        }

        HelperUtils.setInt(0)
        return binding.root
    }

    companion object {
        fun newInstance() = HomeFragment()
    }
}