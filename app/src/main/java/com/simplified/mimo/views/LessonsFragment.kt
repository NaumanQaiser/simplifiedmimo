package com.simplified.mimo.views

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.simplified.mimo.R
import com.simplified.mimo.adapters.LessonsPagerAdapter
import com.simplified.mimo.databinding.LessonsFragmentBinding
import com.simplified.mimo.models.Lesson
import com.simplified.mimo.room.models.LessonCompletionEvent
import com.simplified.mimo.utils.HelperUtils
import com.simplified.mimo.viewmodels.LessonsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import kotlin.collections.ArrayList

@AndroidEntryPoint
class LessonsFragment : Fragment(), LessonsPagerAdapter.OnItemClickListener {

    private val viewModel: LessonsViewModel by viewModels()
    private lateinit var binding: LessonsFragmentBinding
    private lateinit var pagerAdapter: LessonsPagerAdapter
    lateinit var fragManager: FragmentManager
    var lessons = ArrayList<Lesson>()

    companion object {
        fun newInstance() = LessonsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = LessonsFragmentBinding.inflate(layoutInflater)
        fragManager = requireActivity().supportFragmentManager
        binding.viewPager.setOnTouchListener({ v, event -> true })
        setViewModelObservers()
        return binding.root
    }

    fun setViewModelObservers(){

        viewModel.getLessons().observe(viewLifecycleOwner) {
            Log.v("response", it.toString())
            lessons = it.lessons
            pagerAdapter = LessonsPagerAdapter(lessons, requireActivity(), this)
            binding.viewPager.adapter = pagerAdapter
            binding.viewPager.setCurrentItem(HelperUtils.getInt())
            pagerAdapter.notifyDataSetChanged()
        }

        viewModel.errorBody.observe(viewLifecycleOwner) { errorHolder -> showErrorSnackbar(errorHolder.message) }

        viewModel._isLoading.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                binding.loadingProgress.setVisibility(View.VISIBLE)
            } else {
                binding.loadingProgress.setVisibility(View.GONE)
            }
        }

    }
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    override fun onItemClick(next_prev: Int, startTimeStamp: String, lesson: Lesson) {
        if (next_prev == 1) {
            if (binding.viewPager.currentItem == lessons.size - 1) {
                val transaction = requireActivity().supportFragmentManager.beginTransaction()
                transaction.replace(R.id.frameLayout, HomeFragment.newInstance())
                transaction.disallowAddToBackStack()
                transaction.commit()
            } else {
                binding.viewPager.currentItem = binding.viewPager.currentItem + 1
            }

            insertNewLessonCompletionEvent(lesson.id, startTimeStamp)
            HelperUtils.setInt(binding.viewPager.currentItem)

        } else {
            binding.viewPager.currentItem = binding.viewPager.currentItem - 1
        }
    }


    fun insertNewLessonCompletionEvent(id: Int, currentLessonStartingTimestamp: String) {
        // create a lesson completion event object and insert it into the database
        val lessonCompletionEvent = LessonCompletionEvent(
            lessonId = id,
            startingTimestamp = currentLessonStartingTimestamp,
            completionTimestamp = Date().toString()
        )
        viewModel.insertNewLessonCompletionEvent(lessonCompletionEvent)
    }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    fun showErrorSnackbar(message: String?) {
        val snackbar = Snackbar.make(binding.root, message!!, Snackbar.LENGTH_INDEFINITE)
        snackbar.show()
        snackbar.setAction("RETRY") { v: View? ->
            snackbar.dismiss()
            setViewModelObservers()
        }
    }
}