package com.simplified.mimo.repositories

import com.simplified.mimo.models.LessonsApiResponse
import com.simplified.mimo.apiservices.RetrofitBuilder
import com.simplified.mimo.room.DatabaseService
import com.simplified.mimo.room.models.LessonCompletionEvent
import io.reactivex.Observable

class LessonsRepository {
    private val apiService = RetrofitBuilder.apiService

    suspend fun getLessons(): LessonsApiResponse {
        return apiService.getLessons()
    }

    suspend fun insertNewLessonCompletionEvent(lessonCompletionEvent: LessonCompletionEvent){
        // call the database service to insert a new lesson completion event record into the database
        DatabaseService.lessonCompletionEventDao.insertLessonCompletionEvent(lessonCompletionEvent)
    }
}