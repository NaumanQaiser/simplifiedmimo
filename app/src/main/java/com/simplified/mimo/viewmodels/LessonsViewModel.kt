package com.simplified.mimo.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.simplified.mimo.models.ErrorHolder
import com.simplified.mimo.models.LessonsApiResponse
import com.simplified.mimo.repositories.LessonsRepository
import com.simplified.mimo.room.models.LessonCompletionEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

@HiltViewModel
class LessonsViewModel : ViewModel() {
    private val lessonsRepo = LessonsRepository()
    val errorBody: MutableLiveData<ErrorHolder> = MutableLiveData<ErrorHolder>()
    val _isLoading = MutableLiveData<Boolean>()

    fun getLessons() =
        liveData(Dispatchers.IO) {
            _isLoading.postValue(true)
            try {
                val res = lessonsRepo.getLessons()
                emit(res)

            } catch (e: Exception) {
                val errorMessage = "Unable to complete your request! Check Connection"
                errorBody.postValue(ErrorHolder(errorMessage, 500))

            }
            _isLoading.postValue(false)
        }

    fun insertNewLessonCompletionEvent(lessonCompletionEvent: LessonCompletionEvent) {
        viewModelScope.launch {
            // ask the repository to insert a new lesson completion event record into the database
            lessonsRepo.insertNewLessonCompletionEvent(lessonCompletionEvent)
        }
    }

}