package com.simplified.mimo.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.viewpager.widget.PagerAdapter
import com.simplified.mimo.R
import com.simplified.mimo.models.Lesson
import com.simplified.mimo.models.allContents
import com.simplified.mimo.models.contentsLength
import com.simplified.mimo.models.hasInteraction
import com.simplified.mimo.utils.HelperUtils
import com.simplified.mimo.views.LessonsFragment
import java.util.*


class LessonsPagerAdapter(
    private var lessons: ArrayList<Lesson>, val context: Context,
    private val listener: LessonsFragment
) : PagerAdapter() {

    private lateinit var editBox: EditText
    private lateinit var allContents: String
    private var startIndex: Int = 0
    private var endIndex: Int = 0
    private lateinit var nextButton: Button
    var startTimeStamp: String = ""

    override fun isViewFromObject(v: View, `object`: Any): Boolean {
        // Return the current view
        return v === `object` as View
    }

    override fun getCount(): Int {
        // Count the items and return it
        return lessons.size
    }

    override fun instantiateItem(parent: ViewGroup, position: Int): Any {
        // Get the view from pager page layout
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.page_layout, parent, false)
        startTimeStamp = Date().toString()

        // Get the widgets reference from layout
        val linearLayout: LinearLayout = view.findViewById(R.id.linear_layout)
        val tvFooter: TextView = view.findViewById(R.id.tv_footer)
        nextButton = view.findViewById(R.id.next)
        val prevButton: Button = view.findViewById(R.id.prev)

        nextButton.setOnClickListener {
            listener.onItemClick(
                1,
                startTimeStamp,
                lessons[position]
            )
        }

        prevButton.setOnClickListener {
            listener.onItemClick(
                2,
                startTimeStamp,
                lessons[position]
            )
        }
        Log.v("AllContents", lessons.get(position).allContents())
        Toast.makeText(context, lessons.get(position).allContents(), Toast.LENGTH_SHORT).show()

        if (!lessons.get(position).hasInteraction()) {
            for (item in lessons.get(position).contents) {
                val text = HelperUtils.customTextView(context)
                text.text = item.text
                text.setTextColor(Color.parseColor(item.color))
                linearLayout.addView(text)
            }
        } else {

            allContents = lessons.get(position).allContents()
            nextButton.isEnabled = false
            var currentlength = 0
            startIndex = lessons[position].input?.startIndex!!
            endIndex = lessons[position].input?.endIndex!!
            var foundInput = false
            var startIndexInputContent = 0

            for (item in lessons[position].contents) {
                currentlength += item.text.length
                if (currentlength > startIndex!! && !foundInput) {
                    startIndexInputContent = currentlength - item.text.length
                    if (startIndexInputContent == startIndex) {
                        editBox = HelperUtils.customEditText(context)
                        editBox.setTextColor(Color.parseColor(item.color))
                        editBox.setBackgroundResource(R.drawable.rectangle)
                        editBox.isPressed
                        linearLayout.addView(editBox)

                        val remaingString = allContents.substring(endIndex!!, currentlength)
                        val text = HelperUtils.customTextView(context)
                        text.text = remaingString
                        text.setTextColor(Color.parseColor(item.color))
                        linearLayout.addView(text)
                        foundInput = true
                        editBoxTextListner()
                    } else {
                        //Todo 2 conditions if iput text is some middel text in content or at the end of the content
                    }
                } else {

                    val text = HelperUtils.customTextView(context)
                    text.text = item.text
                    text.setTextColor(Color.parseColor(item.color))
                    linearLayout.addView(text)
                }
            }
        }

        tvFooter.text = "Lesson ${position + 1} of ${lessons.size}"

        // Add the view to the parent
        parent.addView(view)

        // Return the view
        return view
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }

    interface OnItemClickListener {
        fun onItemClick(next_prev: Int, startTime: String, lesson: Lesson)
    }

    private fun editBoxTextListner() {
        editBox.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val string = allContents.substring(startIndex, endIndex)
                if (string.contentEquals(s.toString())) {
                    nextButton.isEnabled = true
                } else {
                    Log.v("", "")
                    nextButton.isEnabled = false
                }
            }
        })
    }
}
