package com.simplified.mimo.models

data class LessonsApiResponse(
    val lessons: ArrayList<Lesson>
)