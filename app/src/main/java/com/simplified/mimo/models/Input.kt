package com.simplified.mimo.models

data class Input(
    val startIndex: Int,
    val endIndex: Int
)