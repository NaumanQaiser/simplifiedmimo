package com.simplified.mimo.models

data class Content(
    val color: String,
    val text: String
)