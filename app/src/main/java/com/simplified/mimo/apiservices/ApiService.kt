package com.simplified.mimo.apiservices

import com.simplified.mimo.models.LessonsApiResponse
import retrofit2.http.GET

interface ApiService {

    @GET("lessons")
    suspend fun getLessons(): LessonsApiResponse
}