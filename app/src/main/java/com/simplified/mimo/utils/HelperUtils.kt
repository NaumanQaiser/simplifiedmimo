package com.simplified.mimo.utils

import android.content.Context
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import com.simplified.mimo.MimoApp
import com.simplified.mimo.models.Content

object HelperUtils {

    fun customTextView(context: Context): TextView {
        val textView = TextView(context)
        textView.textSize = 30.0F
        textView.setLayoutParams(
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        return textView
    }

    fun customEditText(context: Context): EditText {

        val editText = EditText(context)
        editText.setPadding(30, 5, 30, 5)
        editText.setLayoutParams(
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        return editText
    }

    fun setInt(lessonposition: Int) {
        val PREFERENCE_NAME = "SharedPreference"
        val preference =
            MimoApp.appContext?.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        val editor = preference?.edit()
        editor?.putInt(PREFERENCE_NAME, lessonposition)
        editor?.commit()
    }

    fun getInt(): Int {
        val PREFERENCE_NAME = "SharedPreference"
        val preference =
            MimoApp.appContext?.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE)
        return preference?.getInt(PREFERENCE_NAME, 0)!!
    }
}