package com.simplified.mimo

import junit.framework.TestCase
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MainActivityTest : TestCase(){
    var activity: MainActivity? = null

    @Before
    @Throws(Exception::class)
    public override fun setUp() {
        activity = Robolectric.buildActivity(MainActivity::class.java)
            .create()
            .resume()
            .get()
    }

    @Test
    open fun checkActivityNotNull() {
        assertNotNull(activity)
    }

    @Test
    @Throws(Exception::class)
    open fun shouldHaveCorrectAppName() {
        val hello = activity?.getResources()?.getString(R.string.app_name)
        MatcherAssert.assertThat(hello, CoreMatchers.equalTo("Mimo Demo"))
    }
}