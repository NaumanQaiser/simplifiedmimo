package com.simplified.mimo.views

import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.simplified.mimo.MainActivity
import com.simplified.mimo.MimoApp
import com.simplified.mimo.R
import com.simplified.mimo.adapters.LessonsPagerAdapter
import com.simplified.mimo.databinding.LessonsFragmentBinding
import com.simplified.mimo.models.Lesson
import com.simplified.mimo.models.LessonsApiResponse
import com.simplified.mimo.room.DatabaseService
import com.simplified.mimo.utils.HelperUtils
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockkStatic
import junit.framework.TestCase
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.Robolectric
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class LessonsFragmentTest : TestCase(){

    private var activity: FragmentActivity? = null
    var fragment: LessonsFragmentTest? = null
    @MockK lateinit var mockCallback: LessonsPagerAdapter.OnItemClickListener

   // @MockK lateinit var binding: LessonsFragmentBinding
    @MockK  lateinit var layoutInflater: LayoutInflater
    @MockK private lateinit var pagerAdapter: LessonsPagerAdapter
    @MockK private lateinit var lesson: Lesson
    @MockK private lateinit var lessons: ArrayList<Lesson>

    @Before
    public override fun setUp() {
        MockKAnnotations.init(this, relaxed = true)

        startFragment(fragment)
    }

    fun startFragment(fragment: LessonsFragmentTest?) {
        activity = Robolectric.buildActivity(MainActivity::class.java)
            .create()
            .start()
            .resume()
            .get()
        val mFragment: Fragment = LessonsFragment()
        val fragmentManager = activity?.supportFragmentManager
        fragmentManager?.beginTransaction()
            ?.replace(R.id.frameLayout, mFragment)?.commit()

     //   mockkStatic(LayoutInflater::class)
     //   every { LayoutInflater.from(any()) } returns layoutInflater

//       binding = LessonsFragmentBinding.inflate(layoutInflater)
     //   pagerAdapter = LessonsPagerAdapter(lessons , MimoApp.appContext, LessonsFragment())
    }

    @Test
    fun shouldNotBeNull() {
        startFragment(fragment)
        assertNotNull(fragment)
    }

    @Test
    fun showSnack() {
        startFragment(fragment)
        fragment?.showSnack()
        assertTrue(true)
    }
    @Test
    fun onNextPrevClick(){
        mockCallback.onItemClick(1,"time",lesson)
        assertEquals(0, HelperUtils.getInt())
    }
    @Test
    fun checkDatabase(){
       val newfragment = LessonsFragment.newInstance()
        newfragment.insertNewLessonCompletionEvent(1,"time")
        assertTrue(DatabaseService.lessonCompletionEventDao.loadAllCompletedLessons().size > 0)
    }
}
