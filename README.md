# README #

### What is this repository for? ###
* Mimo Home assignment
Write a small app that can display a few very simple Mimo lessons. You get the information for the lessons from an API and should display the first lesson. 
The lessons have a specific format (documented later) and should be rendered to the screen. If the lesson contains an interaction, display the interaction.
Also display a "Next" button that, when pressed, checks that the lesson has been solved and continues to the next lesson.


### Reactive MVVM Architecture - Retrofit - Viewpager - Hilt-Dependency Injection - Room - Robolectric###

**Application Flow

Get Lessons from Mimo RestApi using Retrofit
Used Viewpager in the fragment to show Lessons and manage the user interactions
User can traverse to the previous solved lessons also
Once all the lessons are complete user will be redirected to a Complete or home screen. User can start again.
Current lesson is saved in the shared prefreces and user starts lessons from where he has left.

Complete lesson is saved to the database with timestamps

**Models  
- LessonsApiResponse

**ViewModel 
- LessonsViewModel

**View 
- LessonsFragment

**Hilt 
- App is an hilt application with entry point as LessonsFragment which injects ViewModel

### Assumptions ###
Input would be part of only one of the contents
Input is an object not an array in the Json that means there could only be one inputs in all cases


### Tests - Robolectric - Mockk ###*
Test written for Main Activity, LessonsFragment but are incomplete due to time constraint 

### Todo's ###
* Remaining Tests

### Screenshot ###

![Alt text](/SimplifiedMimo/Screenshot_1629433503.PNG?raw=true "Main Searchable Screen")